import React from 'react';
import './Circle.css';


const Circle = props => {
  return (
    <div className="Circle-crl">
      {props.number}
    </div>
  );
};

export default Circle;