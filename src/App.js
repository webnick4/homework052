import React, { Component } from 'react';
import './App.css';
import './Circle/Circle.css';
import Circle from './Circle/Circle';

class App extends Component {

  state = {
    numbers: [
      {number: 0},
      {number: 0},
      {number: 0},
      {number: 0},
      {number: 0}
    ]
  };

  sortNumbers = (val1, val2) => {
    if ((val1 === null || val2 === null) || (typeof val1 !== typeof val2)) {
      return null;
    }

    if (typeof val1 === 'string') {
      return (val1).localeCompare(val2);
    } else {
      if (val1 > val2) {
        return 1;
      } else if (val1 < val2) {
        return -1;
      }
      return 0;
    }
  };

  changeNumbers = (index) => {
    const lottery = {...this.state.numbers[index]};
    lottery.number = [];

    let counter = 5;

    while (counter > 0) {
      let getRandomNumber = Math.floor(Math.random() * (36 - 5 + 1)) + 5;

      if (lottery.number.indexOf(getRandomNumber) === -1) {
        const numbers = [...this.state.numbers];
        numbers[index] = lottery.number.push(getRandomNumber);
        counter--;
      }
    }

    const numbers = lottery.number.sort(this.sortNumbers).map(number => {
      return { number }
    });

    this.setState({ numbers });
  };

  render() {
    return (
        <div className="App-container">
          <div className="App-block">
            <button className="App-btn" onClick={this.changeNumbers}>New numbers</button>
            <div className="App-block-crl">
              {
                this.state.numbers.map((num, index) => {
                  return (
                    <Circle number={num.number} key={index} />
                  );
                })
              }
            </div>
          </div>
        </div>
    );
  }
}

export default App;